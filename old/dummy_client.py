# script to create and delete files to simulate NFS client
import os
import random
from datetime import datetime
from time import sleep

DIR = "/mnt/sharedfolder_client"
counter = 0
FILE_NUM = 3

while True:
    print("// =============================================")
    files = os.listdir(DIR)
    dir_size = len(files)
    diff = dir_size - FILE_NUM
    if diff < 0:
        diff = 0
    elif diff == 0:
        diff = 1

    print("COUNTER:\t\t" + str(counter))
    print("FILES (PRE):\t\t" + str(files))
    print("DELETING:\t\t" + str(diff) + " file(s) in " + DIR)

    if dir_size - FILE_NUM >= 0:
        for file in files[FILE_NUM - 1:]:
            try:
                path = os.path.join(DIR, file)
                print("\tREMOVING:\t\t" + path)
                os.remove(path)
            except Exception as e:
                print("\tEXCEPTION:\t" + str(e))

    NEW_FILE = os.path.join(DIR, "test-" + str(counter) + ".txt")
    print("CREATING:\t\t" + str(NEW_FILE) + " file in " + DIR)
    f = open(NEW_FILE, "w+")
    f.write("COUNTER:\t\t" + str(counter)
            + "\nFILE:\t\t\t" + NEW_FILE
            + "\nCREATED:\t\t" + str(datetime.now()))

    files = os.listdir(DIR)
    print("FILES (POST):\t\t" + str(files))
    sleep(5)
    counter += 1

