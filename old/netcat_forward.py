import os
import subprocess


def netcat_forward(remote_ip, port, type):
    print("starting netcat forwarder to " + str(remote_ip) + ":" + str(port))
    pipe_name = "backpipe_" + str(port)
    if os.path.isfile(pipe_name):  # delete pipe if already exists
        print("pipe file already exists, deleting...")
        cmd = "sudo rm " + pipe_name
        print("\t\t" + cmd)
        process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)

    cmd = "sudo mknod " + pipe_name + " p"
    print("\t" + cmd)
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stderr = process.stderr.read()
    if stderr:
        print("\tERR: " + str(stderr))

    cmd = "sudo ncat "
    if type != "TCP":
        cmd += " -u "
    cmd += "-l -p " + str(port) + " -c \'sudo ncat "
    if type != "TCP":
        cmd += " -u "
    cmd += str(remote_ip) + " " + str(port) + "\'"
    print("\t" + cmd)
    process = subprocess.Popen(cmd, shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    stdout = process.stdout.read()
    print("OUT: " + str(stdout))
    stderr = process.stderr.read()
    if stderr:
        print("\tERR: " + str(stderr))
