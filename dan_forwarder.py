import _thread
import socket
import threading

from scapy.compat import raw

from nfs_mitm_api import NfsMitmApi
from scapy_mount import MOUNT_Call
from scapy_rpc import RPC


def tuple_to_addr(address):
    return str(address[0]) + ":" + str(address[1])


def handle(buffer):
    return buffer


class DanForwarder:
    mount_ports = []
    spoof_address = None

    def __init__(self, remote_ip, port, udp=False):
        try:
            print("// ========================================")
            print("// Starting DanForwarder...")
            print("// [*] LOC Addr:\t127.0.0.1:" + str(port))
            print("// [*] REM Addr:\t" + remote_ip + ":" + str(port))
            if udp:
                print("// [*] UDP:\t\t" + str(udp))
            print("// ========================================")

            if udp:
                self.udp_proxy(remote_ip, port)
            else:
                self.tcp_proxy(remote_ip, port, 1)
        except Exception as e:
            str("[- EXP] " + str(e))

    def update_spoof_address(self, addr):
        if self.spoof_address is None or self.spoof_address is '127.0.0.1' or self.spoof_address is '0.0.0.0' or self.spoof_address is '':
            self.spoof_address = addr

    # ==================== TCP FORWARDING ==================== #

    # def tcp_proxy(self, target_host, target_port, max_connection):
    #     # sniff(prn=self.handle_tcp, store=False, filter="tcp and host " + target_host + " and port " + str(target_port))
    #     s = threading.Thread(target=sniff, kwargs={
    #         'prn': self.handle_tcp,
    #         'store': False,
    #         'filter': "tcp and host " + target_host + " and port " + str(target_port),
    #     })
    #
    # def handle_tcp(self, pkt):
    #     pkt[IP].src =
    #     sendp(pkt)

    def tcp_proxy(self, target_host, target_port, max_connection):
        server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        server_socket.bind(('', target_port))
        server_socket.listen(max_connection)

        while True:
            local_socket, local_address = server_socket.accept()
            self.update_spoof_address(local_address[0])

            remote_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            remote_socket.bind(('', local_address[1]))
            remote_socket.connect((target_host, target_port))

            s = threading.Thread(target=self.transfer_tcp, args=(remote_socket, local_socket))
            r = threading.Thread(target=self.transfer_tcp, args=(local_socket, remote_socket))
            s.start()
            r.start()

    def transfer_tcp(self, src, dst):
        while True:
            data = src.recv(64512)
            print("[+ TCP ] " + tuple_to_addr(src.getpeername()) + " >>> " + tuple_to_addr(dst.getpeername()) + " [" + str(len(data)) + "]")

            if len(data) == 0:
                # print("[- TCP ] No data received. Breaking...")
                break
            # else:
            #     scapy_packet = IP(Raw(data))
            #     recv_ip = scapy_packet[IP].src
            #     print(">> TCP: PACKET RECEIVED FROM " + recv_ip + ", updating IP to: " + self.spoof_address)
            #     packet = IP(src=self.spoof_address) / TCP() / Raw(data)
            # print "[+] %s:%d => %s:%d [%s]" % (src_address, src_port, dst_address, dst_port, repr(buffer))
            dst.send(handle(data))

    # ==================== UDP FORWARDING ==================== #

    def udp_proxy(self, remote_ip, listen_port):
        proxy_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        proxy_socket.bind(('', listen_port))

        server_address = (remote_ip, int(listen_port))
        client_address = None

        while True:
            data, address = proxy_socket.recvfrom(65412)
            self.filter_packets(data, remote_ip, client_address, server_address)

            #  create spoof socket to send packets from
            spoof_socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
            spoof_socket.bind(('', address[1]))

            #  update src IP if from localhost
            # scapy_packet = IP(UDP(Raw(data)))
            # scapy_packet = IP(Raw(data))
            # recv_ip = scapy_packet[IP].src
            # print(">> TCP: PACKET RECEIVED FROM " + recv_ip + ", updating IP to: " + self.spoof_address)
            # scapy_packet[IP].src = self.spoof_address

            if client_address is None:
                client_address = address

            if address == client_address:  # client addr, send to server (listen on spoof socket for resp)
                print(
                    "[+ UDP ] " + tuple_to_addr(client_address) + " >>> " + tuple_to_addr(server_address) + " [" + str(
                        len(data)) + "]")
                spoof_socket.sendto(data, server_address)
                self.udp_listen(spoof_socket, proxy_socket)
            elif address == server_address:  # server addr, send to client
                print(
                    "[+ UDP ] " + tuple_to_addr(server_address) + " >>> " + tuple_to_addr(client_address) + " [" + str(
                        len(data)) + "]")
                proxy_socket.sendto(data, client_address)
                client_address = None
            else:  # unknown addr, send to server
                print(
                    "[+ UDP ] " + tuple_to_addr(server_address) + " >>> " + tuple_to_addr(client_address) + " [" + str(
                        len(data)) + "]")
                proxy_socket.sendto(data, client_address)
                client_address = None

    @staticmethod
    def udp_listen(src, dst):
        data, address = src.recvfrom(65412)
        src.sendto(data, dst.getsockname())

    def filter_packets(self, data, remote_ip, client_address, server_address):
        port = self.filter_mount_port(data)
        if port != -1 and port not in self.mount_ports and client_address is not server_address:
            print("[* INF ] starting forwarders on port " + str(port))
            _thread.start_new_thread(DanForwarder, (remote_ip, port))
            _thread.start_new_thread(DanForwarder, (remote_ip, port, True))
            self.mount_ports.append(port)

        # filter path, start mitm API
        path = self.filter_mount_path(data)
        if path != "NULL":
            print("[* INF ] starting NFS MITM API on path \'" + path + "\'")
            _thread.start_new_thread(NfsMitmApi, (remote_ip, path))

    @staticmethod
    def filter_mount_port(data):
        try:
            # get port # from last few bytes
            sz = len(data)
            x = data[sz - 2:sz]
            tmp = x.hex()
            port = int(tmp, 16)

            if port != 0 and port > 100:
                print("[* INF ] MOUNT on port " + str(port))
                return port
            else:
                return -1
        except Exception as e:
            # str("[- EXP ] " + str(e))
            return -1

    @staticmethod
    def filter_mount_path(data):
        try:
            mnt_pckt = RPC(MOUNT_Call(raw(data)))
            path = mnt_pckt.path.path.decode('utf-8')
            print("[* INF ] MOUNT on path " + path)
            return path
        except Exception as e:
            # print("[- EXP ] " + str(e))
            return "NULL"


# ==================== MAIN ==================== #


if __name__ == '__main__':
    import optparse

    parser = optparse.OptionParser()

    parser.add_option(
        '-r', '--remote-ip', dest='remote_ip',
        help='Local IP address to bind to')
    parser.add_option(
        '-p', '--port',
        type='int', dest='port', default=80,
        help='Port to bind to')
    options, args = parser.parse_args()

    DanForwarder(options.remote_ip, options.port)
    lock = _thread.allocate_lock()
    lock.acquire()
    lock.acquire()
