import _thread
from _signal import SIGTERM
from datetime import datetime

from psutil import process_iter

from dan_forwarder import DanForwarder

PORT_RPC = 111
PORT_NFS = 2049
TIMEOUT = 5


def port_killer(port):
    found = False
    try:
        for proc in process_iter():
            for conns in proc.connections(kind='inet'):
                if conns.laddr.port == port:
                    found = True
                    print("[* INF ] killing process on port " + str(port))
                    proc.send_signal(SIGTERM)
        if not found:
            print("[* INF ] no process found to kill on port " + str(port))
    except:
        print("[* EXP ] exception while trying to kill process on port " + str(port))


def run(ip_nfs_server):
    print("// ========================================================================")
    print("Starting NFS MITM @ " + str(datetime.now()) + "\n")
    print("SERVER IP:\t\t" + ip_nfs_server)
    print("NFS PORT:\t\t" + str(PORT_NFS))
    print("")

    # kill processes on either port just in case
    port_killer(PORT_NFS)
    port_killer(PORT_RPC)

    # start forwarders on 111 (portmapper) and 2049 (nfs)
    _thread.start_new_thread(DanForwarder, (ip_nfs_server, PORT_NFS, False))
    _thread.start_new_thread(DanForwarder, (ip_nfs_server, PORT_NFS, True))
    _thread.start_new_thread(DanForwarder, (ip_nfs_server, PORT_RPC, False))
    _thread.start_new_thread(DanForwarder, (ip_nfs_server, PORT_RPC, True))

    lock = _thread.allocate_lock()
    lock.acquire()
    lock.acquire()


if __name__ == '__main__':
    import optparse
    parser = optparse.OptionParser()

    parser.add_option(
        '-s', '--server-ip', dest='server_ip',
        help='NFS server IP address to bind to')

    options, args = parser.parse_args()

    run(options.server_ip)
