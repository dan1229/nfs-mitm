# NFS MITM
## Daniel Nazarian
## CIS4914 - Senior Project / Spring 2020

Project Contents
- nfs_mitm.py: main script
    - s : server ip address
- dan_forwarder.py: multi-threaded TCP/UDP port forwarder
    - s : server ip address
    - p : port
    - udp : boolean, defaults to False
- nfs_mitm_api.py: API to distribute mount credentials
    - s : server ip address
    - m : mount point
- mitm_client.py: text based client application
    - s : server ip address
    - m : mount point, will get from API if active connection and left empty
- Scapy files: extra files used for Scapy packet filtering